var express = require('express');
var router = express.Router();
var passport = require('passport')
var basicAuth = passport.authenticate('basic', { session: false })
var overlay = require('../controllers/overlayControllers');
var path = require("path");
  

router.get('/',overlay.getDefault);

//router.get('/:num_latest_feeds/',overlay.getNumLatestFeeds);

/*
router.get('/:feed_pubkey/json/', data_feeds.getJSON);

router.get('/:feed_pubkey/csv/',data_feeds.getCSV);

router.post('/:feed_pubkey/', data_feeds.postNewMeasurement);

router.get('/:feed_pubkey/latest/',data_feeds.getLatestMeasurement);
*/

module.exports = router;