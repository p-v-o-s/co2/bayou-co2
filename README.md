# bayou-co2

## usage

### clearing feed of historical data:

```
http://data.pvos.org/co2/manage/clear/[PUB KEY]/[PRIV KEY]
```

## postgres setup

Need a .env file as per example_env:

```
PORT=3006
DB_NAME=hab3
DB_USER=postgres
DB_PASSWORD=pcat999
DB_HOST=localhost
DB_PORT=5432
BASE_URL=data.pvos.org
```

Then need to create the tables 'feeds' and 'measurements':


```
sudo -i -u postgres
postgres@raspberrypi:~$ createdb hab3
postgres@raspberrypi:~$ psql hab3


CREATE TABLE feeds(
    feed_id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    public_key VARCHAR(255) UNIQUE,
    private_key VARCHAR(255) 
);

CREATE TABLE measurements(
id SERIAL PRIMARY KEY,
feed_id INT,
co2 FLOAT,
tempC FLOAT,
humidity FLOAT,
mic FLOAT,
auxPressure FLOAT,
auxTempC FLOAT,
aux001 FLOAT,
aux002 FLOAT,
log VARCHAR(255),    
created TIMESTAMP DEFAULT NOW(),
CONSTRAINT feed
 FOREIGN KEY(feed_id)
REFERENCES feeds(feed_id)
);

```
